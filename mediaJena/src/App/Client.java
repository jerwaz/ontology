package App;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Client console
 * @author JW
 *
 */
public class Client {

	public static String nickname;
	public static int age;
	public static String political;
	public static boolean isMediaTypeSpecific;
	public static String mediaType;

	public static void main(String[] args) {
		print("Initialisation du programme ...");

		print("Veuillez entrer votre pseudo :");
		Scanner scp = new Scanner(System.in);
		nickname = scp.nextLine();
		print("Bonjour " + nickname);
		boolean ageSet = false;
		while (!ageSet) {
			try {
				print("Veuillez renseigner votre age");
				age = (int) Integer.parseInt(scp.nextLine());
				ageSet = true;
			} catch (Exception e) {
				print("Entr�e incorrecte");
				ageSet = false;
			}
		}
		print("Vous avez " + age + " ans");
		
		boolean politicalSet = false;
		while (!politicalSet) {
				print("Quelle est votre orientation politique ? (Aucune, ExtremeGauche, Gauche, Centre, Droite, ExtremeDroite, autre)");
				Requete.queryPolitical();
				political = scp.nextLine();
				if(political.equals("Aucune") || political.equals("ExtremeGauche") || political.equals("Gauche") || political.equals("Centre") || political.equals("Droite") || political.equals("ExtremeDroite") || political.equals("Autre")){
					politicalSet = true;
				}
		}
		
		print("Souhaite un type de media sp�cifique ? (true, false)");
		boolean typeSet = false;
		while(!typeSet){
			try{
				isMediaTypeSpecific = Boolean.parseBoolean(scp.nextLine());
				typeSet = true;
			} catch (Exception e){
				typeSet = false;
			}
		}
		
		if(!isMediaTypeSpecific){
			mediaType ="none";
			print("Vous ne souhaitez pas de media particulier");
		} else {
			boolean chooseSet = false;
			while (!chooseSet) {
					print("Lequel ? (CanalTV, CanalRadio, Imprime, Internet)");
					Requete.queryMediaType();
					mediaType = scp.nextLine();
					if(mediaType.equals("CanalTV") || mediaType.equals("CanalRadio") || mediaType.equals("Imprime") || mediaType.equals("Internet")){
						chooseSet = true;
					}
			}
		}
		
		print("Les medias qui vous correspondent sont les suivants :");
		Requete.queryUser(mediaType,political);
		
		print("Les medias qui sont partagent des id�es oppos�s sont les suivants :");
		List<String> oppositePolitical = Profiler.oppositePolitical(political);
		for(String s : oppositePolitical){
			Requete.queryUser(mediaType,s);
		}
		scp.close();
		print("Au revoir "+nickname);

	}

	public static void print(String s) {
		Logger.getAnonymousLogger().info(s);
	}

}
