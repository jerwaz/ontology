package App;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;

import utils.ImportOWL;
/**
 * Classe regroupant des m�thode de requ�te de l'ontologie
 * @author JW
 *
 */
public class Requete { 
	/**
	 * Requ�te selon le type de m�dia voulu et le bord politique
	 * @param type de m�dia
	 * @param political
	 * @return String result
	 */
	public static String queryUser(String type, String political){
		InfModel inf = importModel();
		
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		
		String requestContent ="";
		if(type.equals("none")){
		requestContent += 			
				"SELECT ?ans ?prix ?pub WHERE {?ans media:estDiffusePar ?Journal . ?Journal rdf:type media:Redaction . ?Journal media:estOrienteA media:"+political+" . ?ans media:prix ?prix . ?ans media:publicite ?pub }";
		} else if(political.equals("none")) {
			requestContent += "SELECT ?ans ?prix ?pub WHERE {?ans rdf:type media:"+type+" . ?ans media:estDiffusePar ?Journal . ?Journal rdf:type media:Redaction . ?ans media:prix ?prix . ?ans media:publicite ?pub }";
		} else {
				requestContent += "SELECT ?ans ?prix ?pub WHERE {?ans rdf:type media:"+type+" . ?ans media:estDiffusePar ?Journal . ?Journal rdf:type media:Redaction . ?Journal media:estOrienteA media:"+political+" . ?ans media:prix ?prix . ?ans media:publicite ?pub }";
		}
		
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf,query);		
		return requestContent + "\n\n" + type + "\n" + political + "\n" + toPrintableString(toExploitableData(queryResult));
	}
	
	/**
	 * Requ�te selon le public cibl�
	 * @param publicCible
	 * @return String result
	 */
	public static  String queryByPublic(String publicCible){
		InfModel inf = importModel();
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = "SELECT ?ans WHERE {?ans media:sAdresseA media:"+publicCible+"}";
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf, query);
		return requestContent + "\n\n" +publicCible + "\n" + toPrintableString(toExploitableData(queryResult));
	}
	
	/**
	 * Requ�te recherchant des m�dias public qui utilise de la publicit�
	 * @return
	 */
	public static String queryPlacementProduit(){
		InfModel inf = importModel();
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = "SELECT ?ans WHERE {?ans rdf:type media:PlacementDeProduit}";
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf, query);
		return requestContent + "\n\n" +toPrintableString(toExploitableData(queryResult));	
	}
	
	/**
	 * Requ�te selon le type de m�dia
	 * @param type
	 * @return String result
	 */
	public static String queryInstanceOf(String type){
		InfModel inf = importModel();
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = "SELECT ?ans WHERE {?ans rdf:type media:"+type+"}";
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf, query);
		return requestContent + "\n\n" +toPrintableString(toExploitableData(queryResult));			
	}
		
	/**
	 * Requ�te renvoyant touts les individuals de la classe media:OrientationPolitique
	 * @return ObservableList<String> ==> format pour liste d�roulante ComboBox
	 */
	public static ObservableList<String> queryPolitical(){
		InfModel inf = importModel();
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = "SELECT ?ans WHERE {?ans rdf:type media:OrientationPolitique}";
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf, query);
		ObservableList<String> resultToReturn = FXCollections.observableArrayList();
		Map<Integer,Map<String,String>> temp = toExploitableData(queryResult);
		for(int i =0; i < temp.size();i++){
			resultToReturn.add(temp.get(i).get("ans"));
		}
		resultToReturn.add("none");
		
		return resultToReturn;
	}
	
	/**
	 * Requ�te renvoyant toutes les sous-classes de la classe media:MoyenDeDiffusion
	 * @return ObservableList<String> ==> format pour liste d�roulante ComboBox
	 */
	public static ObservableList<String> queryMediaType(){
		InfModel inf = importModel();
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = "SELECT ?ans WHERE {?ans rdfs:subClassOf media:MoyenDeDiffusion}";
		String request = requestHeader + requestContent;
		System.out.println(requestContent);
		Query query = QueryFactory.create(request);
		ResultSet queryResult = querySelect(inf, query);
		ObservableList<String> resultToReturn = FXCollections.observableArrayList();
		Map<Integer,Map<String,String>> temp = toExploitableData(queryResult);
		for(int i =1; i < temp.size();i++){
			resultToReturn.add(temp.get(i).get("ans"));
		}
		resultToReturn.add("none");
		//Filtrage des blankNode via regex
		for(int i=0;i<resultToReturn.size();i++){
			Pattern pattern = Pattern.compile("(([^0-9])([0-9]+))");
			Matcher matcher = pattern.matcher(resultToReturn.get(i));
			if(matcher.find()){
				resultToReturn.remove(i);
				i--;
			}
						
		}
		return resultToReturn;
	}
	
	/**
	 * 
	 * @param model
	 * @param query
	 * @return
	 */
	public static ResultSet querySelect(InfModel model, Query query){
		QueryExecution qExe = QueryExecutionFactory.create(query, model);
		ResultSet results = qExe.execSelect();
		return results;
	}

	/**
	 * 
	 * @return
	 */
	public static InfModel importModel(){
		String ontoFile = "media.owl";
		String dataFile = "data.nt";
		InfModel inf = ImportOWL.infModel(ImportOWL.importModel(ontoFile, dataFile));
		return inf;
	}
	
	/**
	 * M�thode de formatage des r�sultats des requ�tes pour affichage.
	 * @param map
	 * @return String
	 */
	public static String toPrintableString(Map<Integer,Map<String,String>> map){
		String results ="";
		Set<Integer> key = map.keySet();
		for(Integer i : key){
			Set<String> key2 = map.get(i).keySet();
			if( i==0){
				for(String sk : key2){
					results+="|\t"+sk+"\t";
				}
				results += "\n";
			}
			for(String s: key2){
				results +="|\t"+map.get(i).get(s)+"\t";
			}
			results += "\n";
		}
		return results;
	}
	
	/**
	 * M�thode transformant un ResultSet en HashMap
	 * @param rs
	 * @return Map<Integer,Map<String,String>> 
	 */
	public static Map<Integer,Map<String,String>> toExploitableData(ResultSet rs){	
		//JSonification de l'objet
		ByteArrayOutputStream outputStream  = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsJSON(outputStream, rs);
		String json = new String(outputStream.toByteArray());
		
		//R�cup�raton des objets pour le remplissage
		Map<Integer,Map<String,String>> data = new HashMap<>();
		JsonObject toSplit = JSON.parse(json);
		JsonObject header = toSplit.getObj("head");
		JsonArray var = header.get("vars").getAsArray();
		JsonObject results = toSplit.getObj("results");
		JsonArray exploitableResult = results.get("bindings").getAsArray();
		
		//Remplissage de la hashmap
		int itemLength = exploitableResult.size();
		for(int i =0;i<itemLength;i++){	
		Map<String,String> temporalI = new HashMap<>();
		JsonObject lowLevelSplit = exploitableResult.get(i).getAsObject();
		for(int j=0; j<var.size(); j++){
			String key = var.get(j).toString();
			key = key.substring(1, key.length()-1);
			
			JsonObject lastLevelSplit = lowLevelSplit.getObj(key);
			String value = lastLevelSplit.get("value").toString();
			value = value.substring(1, value.length()-1);
			//Si on a une uri, simplification de la chaine
			if(lastLevelSplit.get("type").toString().equals("\"uri\"")){
				int index = value.indexOf('#');
				value = value.substring(index+1);
				
			}
			temporalI.put(key, value);
		}
		data.put(i, temporalI);
		}

		return data;
	}
	
}
