package App;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Profiler
 * @author JW
 *
 */
public class Profiler {
	
	/**
	 * 
	 * @param political
	 * @return
	 */
	public static List<String> oppositePolitical(String political){
		List<String> opposite = new ArrayList<>();
		switch (political) {
			case "Aucun" :
				opposite.add("ExtremeGauche");
				opposite.add("ExtremeDroite");
				break;
			case "ExtremeGauche" :
				opposite.add("aucun");
				opposite.add("centre");
				opposite.add("ExtremeDroite");
				opposite.add("Droite");
				break;
			case "Gauche" :
				opposite.add("Droite");
				opposite.add("ExtremeDroite");
				break;
			case "Centre" :
				opposite.add("ExtremeDroite");
				opposite.add("ExtremeGauche");
				break;
			case "Droite" :
				opposite.add("Gauche");
				opposite.add("ExtremeGauche");
				break;
			case "ExtremeDroite" :
				opposite.add("aucun");
				opposite.add("centre");
				opposite.add("ExtremeGauche");
				opposite.add("Gauche");
				break;
			case "Autre" :
				opposite.add("aucun");
				break;
		}			
		return opposite;
	}
	
	/**
	 * 
	 * @param age
	 * @return
	 */
	public static List<String> typePublic(String age){
		List<String> publicType = new ArrayList<>();
		int ageInt = Integer.parseInt(age);
		if(ageInt > 80){
			publicType.add("tresVieux");
			publicType.add("tousPublic");
		} else if (ageInt > 60) {
			publicType.add("retraites");
			publicType.add("Vieux");
			publicType.add("tousPublic");
		} else if (ageInt > 40) {
			publicType.add("adulte");
			publicType.add("cadre");
			publicType.add("ouvrier");
			publicType.add("menagere");
			publicType.add("tousPublic");
		} else if (ageInt > 20) {
			publicType.add("jeunesActifs");
			publicType.add("adolescent");
			publicType.add("tousPublic");
			publicType.add("jeune");
		} else if (ageInt > 10){
			publicType.add("enfant");
			publicType.add("publicSensible");
			publicType.add("tousPublic");
		} else {
			publicType.add("tousPublic");
		}
		return publicType;
	}

}
