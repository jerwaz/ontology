package gui;

import java.util.List;

import App.Profiler;
import App.Requete;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
/**
 * Client graphique permettant de requ�ter l'ontologie inf�r�e.
 * Elle utilise la librairie graphique JAVAFX
 * @author JW
 *
 */
public class ClientGUI extends Application {

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Client");
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.TOP_LEFT);
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setPadding(new Insets(25, 25, 25, 25));
		
		GridPane masterPane = new GridPane();
		pane.setAlignment(Pos.TOP_LEFT);
		pane.setHgap(1);
		pane.setVgap(1);
		pane.setPadding(new Insets(25, 25, 25, 25));
		Scene scene = new Scene(masterPane, 500, 800);
		masterPane.add(pane, 0, 0);
		
		final TextArea result = new TextArea();
		result.setMinHeight(600);
		result.setMaxWidth(1000);
		result.setWrapText(true);
		result.setEditable(false);
		masterPane.add(result, 0, 1);
		
		
		Text sceneTitle = new Text("Media ontologie");
		sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
		pane.add(sceneTitle, 0, 0, 1, 1);
		
		Label age = new Label("Votre age:");
		pane.add(age, 0, 1);
		final TextField ageField = new TextField();
		pane.add(ageField, 1, 1);
		
		Button ageRequeteBouton = new Button("Lancer");
		pane.add(ageRequeteBouton, 2, 1);
		
		ageRequeteBouton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				result.setText("Requ�te selon l'age\n\n");
				List<String> publicCible = Profiler.typePublic(ageField.getText());
				for(int i=0;i<publicCible.size();i++){
					result.setText(result.getText() + Requete.queryByPublic(publicCible.get(i)) + "\n\n");
				}
				
			}
		});
		
		

		Label typeMedia = new Label("Type de m�dia");
		pane.add(typeMedia, 0, 3);
		final ComboBox typeMediaComboBox = new ComboBox(Requete.queryMediaType());
		pane.add(typeMediaComboBox, 1, 3);

		Label orientPo = new Label("Votre orientation politique:");
		pane.add(orientPo, 0, 5);
		final ComboBox orientPoComboBox = new ComboBox(Requete.queryPolitical());
		pane.add(orientPoComboBox, 1, 5);
		
		Button orientPoRequeteBouton = new Button("Lancer");
		pane.add(orientPoRequeteBouton, 2, 5);
		
		orientPoRequeteBouton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				result.setText("Orientation politique\n\n"+Requete.queryUser(typeMediaComboBox.getSelectionModel().getSelectedItem().toString(), orientPoComboBox.getSelectionModel().getSelectedItem().toString()));
			}
		});
		
		Button requeteBouton = new Button("Lancer la requ�te \"placement de produits\"");
		pane.add(requeteBouton, 1, 10);

		

		requeteBouton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {

				result.setText("requ�te \"placement de produits\"\n\n" + Requete.queryPlacementProduit());
			}
		});

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}