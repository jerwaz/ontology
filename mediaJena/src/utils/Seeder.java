package utils;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Classe de peuplement de l'onthologie
 * @author JW
 *
 */
public class Seeder {

	public static void main(String[] args) {
		String adresse = "http://www.ema.com/onthologies/media";
		try{
		    PrintWriter writer = new PrintWriter("data.nt", "UTF-8");
		    for(int i = 0; i < 20; i++){
		    	writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" + "> ");
			    writer.print("<" + adresse + "#Journal" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#estDiffuseEn" + "> ");
			    writer.print("<" + adresse + "#Anglais" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" + "> ");
			    writer.print("<" + adresse + "#CanalRadio" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#estDiffuseEn" + "> ");
			    if(i%2 == 1){
			    	writer.print("<" + adresse + "#Allemand" + "> ");
			    } else {
			    	writer.print("<" + adresse + "#Anglais" + "> ");
			    }
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#estDiffusePar" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#estDiffusePar" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#sAdresseA" + "> ");
			    writer.print("<" + adresse + "#jeune" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#sAdresseA" + "> ");
			    writer.print("<" + adresse + "#jeune" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#estDisponibleDans" + "> ");
			    writer.print("<" + adresse + "#france" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#estDisponibleDans" + "> ");
			    writer.print("<" + adresse + "#france" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#paraitAUne" + "> ");
			    writer.print("<" + adresse + "#hebdomadaire" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#paraitAUne" + "> ");
			    writer.print("<" + adresse + "#continue" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#PDG"+i + "> ");
			    writer.print("<" + adresse + "#nomine" + "> ");
			    writer.print("<" + adresse + "#redacEnChef" + i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#france" + "> ");
			    writer.print("<" + adresse + "#heberge" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#Droite" + "> ");
			    writer.print("<" + adresse + "#estLOrientationDe" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.print("<" + adresse + "#aLeStatut" + "> ");
			    writer.print("<" + adresse + "#public" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#redacEnChef"+i + "> ");
			    writer.print("<" + adresse + "#dirige" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#concerne" + "> ");
			    writer.print("<" + adresse + "#science" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#concerne" + "> ");
			    writer.print("<" + adresse + "#politique" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journaliste"+i + "> ");
			    writer.print("<" + adresse + "#compose" + "> ");
			    writer.print("<" + adresse + "#redac"+i + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    writer.print("<" + adresse + "#prix" + "> \"2.0\"^^");
			    writer.print("<" + "http://www.w3.org/2001/XMLSchema#float" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    writer.print("<" + adresse + "#prix" + "> \"0.0\"^^");
			    writer.print("<" + "http://www.w3.org/2001/XMLSchema#float" + "> ");
			    writer.println(".");
			    
			    writer.print("<" + adresse + "#journal"+i + "> ");
			    if(i%2 == 1){
			    	writer.print("<" + adresse + "#publicite" + "> \"false\"^^");
			    }else{
			    	writer.print("<" + adresse + "#publicite" + "> \"true\"^^");
			    }
			    writer.print("<" + "http://www.w3.org/2001/XMLSchema#boolean" + "> ");
			    writer.println(".");
			    writer.print("<" + adresse + "#radio"+i + "> ");
			    if(i%2 == 1){
			    	writer.print("<" + adresse + "#publicite" + "> \"false\"^^");
			    }else{
			    	writer.print("<" + adresse + "#publicite" + "> \"true\"^^");
			    }
			    writer.print("<" + "http://www.w3.org/2001/XMLSchema#boolean" + "> ");
			    writer.println(".");
		    }
		    writer.close();		    
		} catch (IOException e) {
		   System.out.println("// do something");
		}

	}

}
