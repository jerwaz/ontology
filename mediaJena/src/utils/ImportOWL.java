package utils;

import java.io.InputStream;
import java.util.logging.Logger;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.rulesys.RDFSRuleReasonerFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.ReasonerVocabulary;

/**
 * Classe d'import d'onlogie OWL
 * @author JW
 *
 */
public class ImportOWL {
	
	/**
	 * M�thode renvoyant le model associ� � une onthologie OWL
	 * @param modelfile
	 * @param datafile
	 * @return
	 */
	public static Model importModel(String modelfile, String datafile){
		Model model = RDFDataMgr.loadModel(modelfile);
        RDFDataMgr.read(model, datafile);
		return model;
	}
	
	/**
	 * M�thode renvoyant le model inf�r� d'un Model
	 * @param model
	 * @return
	 */
	public static InfModel infModel(Model model){
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
        InfModel inf = ModelFactory.createInfModel(reasoner, model);
		return inf;
	}
	
	/**
	 * M�thode renvoyant un OntModel � partir d'un fichier OWL d'entr�
	 * @param ontoFile
	 * @return
	 */
	public static OntModel importOWL(String ontoFile) {
		OntModel ontoModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
	    try 
	    {
	        InputStream in = FileManager.get().open(ontoFile);
	        try 
	        {
	            ontoModel.read(in, null);
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	        Logger.getAnonymousLogger().info("Ontology " + ontoFile + " loaded.");
	    } 
	    catch (JenaException je) 
	    {
	        System.err.println("ERROR" + je.getMessage());
	        je.printStackTrace();
	        System.exit(0);
	    }
	    return ontoModel;
	}
	
	/**
	 * M�thode renvoyant un InfModel (Un OntModel trait� par un reasoner) � partir d'un fichier OWL d'entr�
	 * @param ontoFile
	 * @return
	 */
	public static InfModel importInfOWL(String ontoFile){
		OntModel ontoModel = importOWL(ontoFile);
		//Model model = RDFDataMgr.loadModel(ontoFile);
		Reasoner reasoner = RDFSRuleReasonerFactory.theInstance().create(null);
		reasoner.setParameter(ReasonerVocabulary.PROPsetRDFSLevel,
		                      ReasonerVocabulary.RDFS_FULL);
		InfModel inf = ModelFactory.createInfModel(reasoner, ontoModel);
		//Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		//InfModel inf = ModelFactory.createInfModel(reasoner,model);
		return inf;
	}
}
