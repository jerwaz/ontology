package utils;

import java.util.List;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;

/**
 * Classe servant � tester des requ�tes SPARQL
 * @author JW
 *
 */
public class TestRequetes {

	public static void main(String[] args) {
		String ontoFile = "media.owl";
		String dataFile = "data.nt";
		InfModel inf = ImportOWL.infModel(ImportOWL.importModel(ontoFile, dataFile));
		 OntModel ontoModel = ImportOWL.importOWL(ontoFile);
		//ontoModel.write(System.out);
		//inf.write(System.out);
		String requestHeader =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX media: <http://www.ema.com/onthologies/media#>";
		String requestContent = 
				// Requete qui renvoie tous
				// "SELECT ?subject ?object"+
				// "WHERE { ?subject rdfs:subClassOf ?object }";

				// Requete qui selectionne les instances de journaux ayant une
				// redaction declar�e
				//"SELECT ?Journal ?prix ?Nom WHERE { ?Journal media:estDiffusePar ?Nom . ?Journal media:prix ?prix}";
				
				
				// Requete qui recup une instance par nom
				//SELECT ?Journal ?prix ?Nom
				// WHERE { ?Journal media:estDiffusePar ?Nom . ?Journal media:prix ?prix . FILTER ( regex(str(?Journal), "LeMonde" ))}
				
				//Requete avec filtrage des types
				"SELECT ?Journal WHERE { ?Journal rdf:type media:Redaction . ?Journal media:estOrienteA media:droite}";
		String request = requestHeader + requestContent;
		Query query = QueryFactory.create(request);
		ResultSetFormatter.out(System.out, querySelect(inf,query), query);
		List<QuerySolution> list = ResultSetFormatter.toList(querySelect(inf,query));
		System.out.println(list.toString());
	}
	
	public static ResultSet querySelect(InfModel model, Query query){
		QueryExecution qExe = QueryExecutionFactory.create(query, model);
		ResultSet results = qExe.execSelect();
		return results;
	}
}